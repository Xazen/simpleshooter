// Fill out your copyright notice in the Description page of Project Settings.


#include "SimpleShooterPlayerController.h"

#include "Blueprint/UserWidget.h"

void ASimpleShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	CrosshairUI = CreateWidget(this, CrosshairWidget);
	if (CrosshairWidget != nullptr)
	{
		CrosshairUI->AddToViewport();
	}
}

void ASimpleShooterPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner)
{
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	CrosshairUI->RemoveFromViewport();
	
	TSubclassOf<UUserWidget> ResultWidget = bIsWinner ? WinScreenWidget : LoseScreenWidget;
	UUserWidget* ResultScreen = CreateWidget(this, ResultWidget);
	if (ResultScreen != nullptr)
	{
		ResultScreen->AddToViewport();
	}
	
	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}
