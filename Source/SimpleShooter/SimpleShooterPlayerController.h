// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "SimpleShooterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_API ASimpleShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	
public:
	virtual void GameHasEnded(AActor* EndGameFocus, bool bIsWinner) override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> CrosshairWidget;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> LoseScreenWidget;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class UUserWidget> WinScreenWidget;
	
	UPROPERTY(EditAnywhere)
	float RestartDelay = 5;

	FTimerHandle RestartTimer;

private:
	UPROPERTY()
	UUserWidget* CrosshairUI;
};
