// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTask_Shoot.h"



#include "AIController.h"
#include "Gun.h"
#include "ShooterCharacter.h"

UMyBTTask_Shoot::UMyBTTask_Shoot()
{
	NodeName = "Shoot";
}

EBTNodeResult::Type UMyBTTask_Shoot::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	AAIController* AIOwner = OwnerComp.GetAIOwner();
	if (AIOwner == nullptr)
	{
		return EBTNodeResult::Failed;
	}

	AShooterCharacter* Character = Cast<AShooterCharacter>(AIOwner->GetPawn());
	if (Character == nullptr)
	{
		return EBTNodeResult::Failed;
	}
	
	Character->Shoot();
	return EBTNodeResult::Succeeded;
}
